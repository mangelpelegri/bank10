0 Screen 1,3: 'Screen mode
1 DefInt A-Z: 'All variables will be of integer type
2 For I = 0 To 23: 'Defines arrow-shaped characters ( % & ' )
3 Read A$
4 VPoke 296+I,Val("&H"+A$)
5 Next I
6 For I= 0 To 95: 'Defines sprites
7 Read A$
8 VPoke Base(9)+I,Val("&H"+A$)
9 Next I

10 Width 24: 'Text width on screen
11 Key Off: 'Hide function keys
12 Locate 8,4:Print "BANK 10" 'Game Title
13 Locate 0,9: 'Set position to start drawing windows
14 For I = 1 To 3:Print " ██████ ";:Next I 'Printing of the upper part of the windows

20 For I = 1 To 5: 'Printing on the sides of the windows
21 For J = 1 To 3:Print " █    █ ";:Next J
22 Next I
23 Print String$(24,219): 'Printing bottom line of the windows
24 Print: 'Line break
25 Print "     %     &      '": 'Printing arrows under windows
26 V=6: 'Initial number of lives
27 P=0: 'Starting points
28 Q=0: 'Initial deaths

30 If Inkey$ = "" Then GoTo 30: 'Waiting for key press
31 For I = 0 To 3
32 Put Sprite I,(0,120),0,9: ' Four sprites to make the exit through the window smoother (4th sprite rule)
33 B(I)=0: 'Initialization of variable with the content of a window 0: Nothing; 1: Customer; 2,3: Gunslinger;
34 Next I

39 'Main loop
40 Locate 0,22:Print "$";P;"Lv";V;"+";Q: 'Prints the scoreboard
41 If V < 1 Then GoTo 90: 'No lives left, end of game
42 T=Stick(0): 'Cursor keystroke capture
43 E=-(T=7)-2*(T=1)-3*(T=3)-1: 'Turns the direction keys, in the window that has been triggered and must be checked (0, 1, 2)
44 If E > -1 Then Beep:M=1:GoSub 80: 'If an address has been pressed, play beep and check what has happened

50 R=Rnd(1)*7: 'Random number to check if someone leaves through the window, and through which window. Values 0,1,2 correspond to window (left to right)
51 D=Rnd(1)*3: 'Random number to check which character goes out the window. 0 customer; 1 and 2: gunman;
52 M=0: 'Does not check keystroke
53 For E=0 To 2
54 C(E)=C(E)-(C(E)>0): 'Updates the character cycle at the window
55 If C(E) >15 Then GoSub 80: 'If it has exceeded 15 cycles, check what has happened

60 Next
61 If R<3 And B(R)=0 Then GoTo 62 Else GoTo 67: 'If the R window is empty it will show character
62 For I = 0 To 32 Step 8
63 Put Sprite R+4,(48+64*R,119-I),9,D: 'Shows the character in the window
64 Next I
65 C(R)=1: 'Upgrade "R" character cycle to 1
66 B(R)=D+1: 'Updates the "R" window character
67 GoTo 40: 'Return to main loop

70 End: 'It should not arrive

79 'Update window states, marker values, and hide sprite
80 B=B(E): 'Temporarily saves the character in the "E" window
81 B(E)=0: 'Updates the "E" window character to 0
82 C(E)=0: 'Upgrade "E" character cycle to 0
83 Put Sprite E+4,(0,209),6,0: 'Hides the "E" window sprite
84 V=V+(M=1 And B<2)+(M=0 And B>1): 'Upgrade lives
85 P=P-(M=0 And B=1)*10: 'Update the points
86 Q=Q-(M=1 And B>1): 'Upgrade dead gunmen
87 Return: 'Returns the execution

90 Locate 8,4:Print "Game Over": 'Print "Game Over" on the screen
91 If Inkey$ = "" Then 91: 'Waiting for key press
92 End: 'End of game

100 Data 0,20,60,FE,60,20,0,0,10,38,7C,10,10,10,10,0,0,4,6,7F,6,4,0,0: ' Character Redefinition by Arrows
110 Data 7,1F,3F,3B,2F,2C,7,3,1F,3D,6E,6D,66,65,6E,3D,80,E0,F0,70,D0,D0,80,0,C0,F0,F8,D8,8C,9E,9E,CC: 'Customer Sprite 
120 Data 2,27,1F,,5,7,7,1,3,F,F,D,F5,19,1,1,40,E4,F8,0,60,E0,E0,80,D8,DC,EC,F6,F6,F6,F6,F6: 'Sprite of the right-handed gunman
130 Data 6,F,3F,1D,F,C,5,3,1F,3F,6F,6F,6F,6F,6F,3F,80,40,F0,60,C0,50,A0,0,C0,F0,F0,B0,AF,98,80,80: 'Sprite of the left-handed gunman

199 'Variables
200 'A$ -> Temporary. Load graphics, wait for keystroke
201 'B -> Previous value of the "R" window character. Take the value of B(R).
202 'B(n) -> Contents of "N" window 0: Nothing; 1: Customer; 2,3: Outlaw;
203 'C(n) -> Cycles completed per character in "N" position
204 'D -> Type of character that will come out of the window 0: Customer, 1,2: Outlaw;
205 'E -> Number of window fired at, number of window being checked
206 'I,J -> Temporary. Iteration
207 'M -> A key has been pressed in the test if it has the value 1
208 'P -> Points
209 'Q -> Dead gunmen 
210 'R -> Random
211 'T -> Temporary. Cursor pulse reading
212 'V -> Remaining lives
