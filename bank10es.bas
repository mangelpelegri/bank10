0 Screen 1,3: 'Modo de pantalla
1 DefInt A-Z: 'Define variables enteras
2 For I = 0 To 23: 'Carga caracteres con forma de flecha ( % & ' )
3 Read A$
4 VPoke 296+I,Val("&H"+A$)
5 Next I
6 For I= 0 To 95: 'Carga sprites
7 Read A$
8 VPoke Base(9)+I,Val("&H"+A$)
9 Next I

10 Width 24: 'Ancho de texto
11 Key Off: 'Oculta teclas de función
12 Locate 8,4:Print "BANK 10" 'Título del juego
13 Locate 0,9: 'Fija posición para empezar a dibujar ventanillas
14 For I = 1 To 3:Print " ██████ ";:Next I 'Impresión parte superior de las ventanillas

20 For I = 1 To 5: 'Impresión laterales de las ventanillas
21 For J = 1 To 3:Print " █    █ ";:Next J
22 Next I
23 Print String$(24,219): 'Impresión línea inferior de las ventanillas
24 Print: 'Espacio
25 Print "     %     &      '": 'Impresión de flechas bajo ventanillas
26 V=6: 'Número inicial de vidas
27 P=0: 'Puntos iniciales
28 Q=0: 'Muertos inciales

30 If Inkey$ = "" Then GoTo130 
31 For I = 0 To 3
32 Put Sprite I,(0,120),0,9: ' Cuatro sprites para hacer más suave la salida por la ventanilla (Regla 4º sprite)
33 B(I)=0: 'Inicialización de variable con el contenido de una ventanilla 0: Nada; 1:Cliente; 2,3: Pistolero;
34 Next I

39 'Bucle principal
40 Locate 0,22:Print "$";P;"Lives";V;"Death";Q: 'Actualiza el marcador
41 If V < 1 Then 190: 'No quedan vidas, finaliza la partida
42 T=Stick(0): 'Captura pulsación de los cursores
43 E=-(T=7)-2*(T=1)-3*(T=3)-1: 'Convierte las direcciones en la ventanilla que se ha disparado y se debe revisar (0, 1, 2)
44 If E > -1 Then Beep:M=1:GoSub 180: 'Si se ha pulsado una dirección comprueba qué ha sucedido

50 R=Rnd(1)*7: 'Número aleatorio para comprobar si sale alguien por ventanilla, y por qué ventanilla. Valores 0,1,2 se corresponden con ventanilla
51 D=Rnd(1)*3: 'Número aleatorio para comprobar qué personaje sale por ventanilla; 0 y 1: pistolero ;2: cliente
52 M=0: 'No comprueba pulsación de tecla
53 For E=0 To 2:
54 C(E)=C(E)-(C(E)>0): 'Actualiza el ciclo del personaje en ventanilla
55 If C(E) >15 Then GoSub 180: 'Si ha superado los 5 ciclos, comprueba qué ha sucedido

60 Next
61 If R<3 And B(R)=0 Then 162 Else 167: 'Si la ventanilla R está vacía mostrará personaje
62 For I = 0 To 32 Step 8
63 Put Sprite R+4,(48+64*R,119-I),6,D: 'Muestra el personaje de la ventanilla
64 Next I
65 C(R)=1: 'Actualiza el ciclo del personaje R a 1
66 B(R)=D+1: 'Actualiza el personaje de la ventanilla R
67 GoTo 140: 'Vuelve al bucle principal

70 End: 'No debería llegar

79 'Actualiza estados de ventanilla, valores del marcador, y oculta sprite
80 B=B(E): 'Guarda temporalmente el personaje de la ventanilla E
81 B(E)=0: 'Actualiza el personaje de la ventanilla E a 0
82 C(E)=0: 'Actualiza el ciclo del personaje de la ventanilla E a 0
83 Put Sprite E+4,(0,209),6,0: 'Oculta el sprite de la ventanilla E
84 V=V+(M=1AndB<2)+(M=0AndB>1): 'Actualiza las vidas
85 P=P-(M=0AndB=1)*10: 'Actualiza los puntos
86 Q=Q-(M=1AndB>1): 'Actualiza los muertos
87 Return: 'Devuelve la ejecución

90 Locate 8,4:Print "Game Over": 'Imprime en pantalla el Game Over
91 If Inkey$ = "" Then 191 Else End: 'Al pulsar una tecla vuelve la ejecución

100 Data 0,20,60,FE,60,20,0,0,10,38,7C,10,10,10,10,0,0,4,6,7F,6,4,0,0: ' Redefinición de caracteres por Flechas
110 Data 7,1F,3F,3B,2F,2C,7,3,1F,3D,6E,6D,66,65,6E,3D,80,E0,F0,70,D0,D0,80,0,C0,F0,F8,D8,8C,9E,9E,CC: 'Sprite cliente
120 Data 2,27,1F,,5,7,7,1,3,F,F,D,F5,19,1,1,40,E4,F8,0,60,E0,E0,80,D8,DC,EC,F6,F6,F6,F6,F6: 'Sprite pistolero diestro
130 Data 6,F,3F,1D,F,C,5,3,1F,3F,6F,6F,6F,6F,6F,3F,80,40,F0,60,C0,50,A0,0,C0,F0,F0,B0,AF,98,80,80: 'Sprite pistolero zurdo

200 'A$ -> Temporal. Cargar gráficos, esperar pulsación de tecla
201 'B -> Valor previo del personaje de la ventanilla R. Toma el valor de B(R)
202 'B(n) -> Contenido de una ventanilla 0: Nada; 1: Cliente; 2,3: Forajido;
203 'C(n) -> Ciclos completados por el enemigo en posición N
204 'D -> Tipo de personaje que saldrá por ventanilla 0: Cliente, 1,2: Forajido;
205 'E -> Ventanilla a la que se ha disparado, ventanilla que se está comprobando
206 'I,J -> Temporal. Iteración
207 'M -> En la comprobación se ha pulsado una tecla si es 1
208 'P -> Puntos
209 'Q -> Pistoleros muertos 
210 'R -> Random
211 'T -> Temporal. Lectura de la pulsación del cursor
212 'V-> Vidas
