# bank10


## Name
It's a game that takes place in a bank and is written in 10 lines of code.

## Description
This is a game written in MSX Basic for MSX system computers.

The game participated in the Tenliner basic contest of homeputerium (https://www.homeputerium.de/) in the year 2023.

The game is an adaptation of the arcade game “Bank Panic”, and the 8-bit computer game “West Bank” by Dinamic.

Among the project files you can find the game code in 10 lines of basic (not to be confused with 10 instructions), as well as a version of the game explained.

You can also find the instructions for the game.

Good luck.


## Usage
The easiest way to play is to load the **bank10.dsk** file in your favorite MSX emulator (OpenMSX, blueMSX, etc.). 

The game will start automatically when the loading is finished. The disk image contains an **autoexec.bas** file with the game. In case it doesn't start automatically, run from basic the instruction:
`run“autoexec.bas`

## Support & Roadmap
The set is delivered as is. No future enhancements are planned.

## Authors and acknowledgment
The game is entirely programmed by Miguel Ángel Pelegrí. 
The graphics are also property of Miguel Ángel Pelegrí.

## License
Attribution-ShareAlike 4.0 International 
CC BY-SA 4.0 DEED

## Project status
Project completed